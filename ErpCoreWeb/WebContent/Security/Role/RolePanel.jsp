<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="java.util.List" %>
<%
CUser m_User = null;
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../../Login.jsp");
    return ;
}
m_User=(CUser)request.getSession().getAttribute("User");
if (!m_User.IsRole("管理员"))
{
	response.getWriter().print("没有管理员权限！");
	response.getWriter().close();
	return ;
}
%>
<%
 CTable m_Table = null;
 CCompany m_Company = null;
 String B_Company_id = request.getParameter("B_Company_id");
 if (Global.IsNullParameter(B_Company_id))
     m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
 else
     m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

 m_Table = (CTable)m_Company.getOrgMgr().getTable();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(function() {
            $("#toptoolbar").ligerToolBar({ items: [
                { text: '增加', click: onAdd, icon: 'add' },
                { line: true },
                { text: '修改', click: onEdit, icon: 'modify' },
                { line: true },
                { text: '删除', click: onDelete, icon: 'delete' }
            ]
            });
        });

        function onAdd() {
            $.ligerDialog.open({ title: '新建', url: 'AddRole.jsp?B_Company_id=<%=request.getParameter("B_Company_id") %>', name: 'winAddRec', height: 200, width: 300, showMax: true, showToggle: true,  isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    dialog.close();
                } }
             ], isResize: true
            });
        }
        function onEdit() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.open({ title: '修改', url: 'EditRole.jsp?id=' + row.id + '&B_Company_id=<%=request.getParameter("B_Company_id") %>', name: 'winEditRec', height: 200, width: 300, showMax: true, showToggle: true,  isResize: true, modal: false, slide: false, 
                buttons: [
                { text: '确定', onclick: function(item, dialog) {

                    var ret = document.getElementById('winEditRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    dialog.close();
                } 
                }
             ], isResize: true
            });
        }
        function onDelete() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.confirm('确认删除？', function(yes) {
                if (yes) {
                    $.post(
                    'RolePanel.do',
                    {
                        Action: 'Delete',
                        delid: row.id,
                        B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                    },
                     function(data) {
                         if (data == "" || data == null) {
                             $.ligerDialog.close();
                             grid.loadData(true);
                             return true;
                         }
                         else {
                             $.ligerDialog.warn(data);
                             return false;
                         }
                     },
                    'text');
                }
            });
        }
    </script>
    <style type="text/css">
    #menu1,.l-menu-shadow{top:30px; left:50px;}
    #menu1{  width:200px;}
    </style>
    
    <script type="text/javascript">
        var grid;
        $(function ()
        {
            grid = $("#gridTable").ligerGrid({
            columns: [
                <%
                List<CBaseObject> lstObjC=m_Table.getColumnMgr().GetList();
                for (int i=0;i<lstObjC.size();i++)
                {
                    CColumn col = (CColumn)lstObjC.get(i);
                    if (col.getCode().equalsIgnoreCase("id"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Created"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Creator"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updated"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updator"))
                        continue;
                    if(i<lstObjC.size()-1)
                    	out.print(String.format("{ display: '%s', name: '%s'},",col.getName(),col.getCode()));
                    else
                    	out.print(String.format("{ display: '%s', name: '%s'}",col.getName(),col.getCode()));
                }
                 %>
                ],
                url: 'RolePanel.do?Action=GetData&B_Company_id=<%=request.getParameter("B_Company_id") %>',
                dataAction: 'server', pageSize: 30,
                width: '100%', height: '100%',
                onSelectRow: function (data, rowindex, rowobj)
                {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                }
            });
        });
        
    </script>
</head>
<body style="padding:6px; overflow:hidden;"> 
  <div id="toptoolbar"></div> 
   <div id="gridTable" style="margin:0; padding:0"></div>

</body>
</html>