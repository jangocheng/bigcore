<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDefMgr" %>
<%@ page import="com.ErpCoreModel.Workflow.ActivesType" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CLinkMgr" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
   
<%
CUser m_User = null;
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
m_User=(CUser)request.getSession().getAttribute("User");
if (!m_User.IsRole("管理员"))
{
	response.getWriter().print("没有管理员权限！");
	response.getWriter().close();
	return ;
}
CCompany m_Company;
String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

String wfid = request.getParameter("wfid");
if (Global.IsNullParameter(wfid))
{
    response.getWriter().close();
    return;
}
CWorkflowDef m_WorkflowDef = (CWorkflowDef)m_Company.getWorkflowDefMgr().Find(Util.GetUUID(wfid));
if (m_WorkflowDef == null) //可能是新建的
{
    if (request.getSession().getAttribute("AddWorkflowDef") == null)
    {
    	response.getWriter().close();
        return;
    }
    m_WorkflowDef = (CWorkflowDef)request.getSession().getAttribute("AddWorkflowDef");
}
String PreActives = request.getParameter("PreActives");
CActivesDef m_PreActives = (CActivesDef)m_WorkflowDef.getActivesDefMgr().Find(Util.GetUUID(PreActives));

CLinkMgr m_LinkMgr = m_WorkflowDef.getLinkMgr();

CTable m_Table = m_LinkMgr.getTable();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    
    <script type="text/javascript">

        function onSubmit() {
            $.post(
                'AddLink.do',
                {
                <% 
                List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
                for (CBaseObject obj : lstCol)
                {
                    CColumn col = (CColumn)obj;
                    if (col.getCode().equalsIgnoreCase("id"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Created"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Creator"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updated"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updator"))
                        continue;
                    
                    if (col.getColType() == ColumnType.bool_type)
                    {
                    %>
                    <%=col.getCode() %>: $("#<%=col.getCode() %>").attr("checked"),
                    <%}
                    else
                    { %>
                    <%=col.getCode() %>: $("#<%=col.getCode() %>").val(),
                    <%}
                  } %>
                    Action: 'PostData',
                    wfid:'<%=request.getParameter("wfid")%>',
                    PreActives:'<%=request.getParameter("PreActives")%>',
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid2.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
        function onCancel() {
            $.post(
                'AddLink.do',
                {
                    Action: 'Cancel',
                    wfid:'<%=request.getParameter("wfid")%>',
                    PreActives:'<%=request.getParameter("PreActives")%>',
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid2.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
        
function btAdd_onclick() {
    if(form1.cbColumn.selectedIndex==-1)
    {
        $.ligerDialog.warn("请选择字段！");
        return false;
    }
    $.post(
        'AddLink.do',
        {
            Action: 'GetCondiction',
            wfid:'<%=request.getParameter("wfid")%>',
            PreActives:'<%=request.getParameter("PreActives")%>',
            Column:$("#cbColumn").val(),
            Val:$("#txtVal").val(),
            B_Company_id:'<%=request.getParameter("B_Company_id") %>'
        },
         function(data) {
             if (data == "" || data == null) {
                 $.ligerDialog.warn("值错误！");
                 return false;
             }
             else {
                 var sExp="";
                 if($("#Condiction").val()!="")
                 {
                    sExp += $("#cbAndOr").val();
                 }
                 sExp += "["+$("#cbColumn").val()+"]";
                 sExp += $("#cbSign").val();
                 sExp += data;
                 
                 sExp = $("#Condiction").val()+sExp;
                 $("#Condiction").val(sExp);
                 return true;
             }
         },
         'text');
}

    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" >
    <div>
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <input name="WF_WorkflowDef_id" type="hidden" id="WF_WorkflowDef_id" value="<%=request.getParameter("wfid") %>"/>
       
            <tr>
                <td align="right" class="l-table-edit-td">前置活动:</td>
                <td align="left" class="l-table-edit-td">
                <input name="PreActives" type="text" id="PreActives" ltype="text" readonly="readonly" value="<%=m_PreActives.getName() %>"/>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">审批结果:</td>
                <td align="left" class="l-table-edit-td">
                <select name="Result" id="Result" ltype="select">
                  <option value="1">接受</option>
                  <option value="2">拒绝</option>
                  </select>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td" valign="top">条件:</td>
                <td align="left" class="l-table-edit-td">
                <table>
                <tr><td>
                <textarea cols="100" rows="4" class="l-textarea" id="Condiction" style="width:400px" ></textarea>
                </td></tr>
                <tr><td>
                    <select name="cbAndOr" id="cbAndOr"  style="width:40px">
                      <option value=" and ">与</option>
                      <option value=" or ">或</option>
                      </select>
                    <select name="cbColumn" id="cbColumn"  style="width:80px">
                        <%                        
                            CTable Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_WorkflowDef.getFW_Table_id());
                        if (Table != null)
                        {
                            List<CBaseObject> lstObj = Table.getColumnMgr().GetList();
                            for (CBaseObject obj : lstObj)
                            {
                                CColumn Column = (CColumn)obj;
                                %>
                      <option value="<%=Column.getName()%>"><%=Column.getName()%></option>
                                <%
                            }
                        }
                         %>
                      </select>
                      <select name="cbSign" id="cbSign"  style="width:40px">
                      <option >=</option>
                      <option >></option>
                      <option >>=</option>
                      <option ><</option>
                      <option ><=</option>
                      <option ><></option>
                      <option >like</option>
                      </select>
                      <input name="txtVal" type="text" id="txtVal"  style="width:80px"/>
                      <input name="btAdd" id="btAdd" type="button" value="添加"  style="width:60px" onclick="return btAdd_onclick()" />
                </td></tr>
                </table>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">后置活动:</td>
                <td align="left" class="l-table-edit-td">
                <select name="NextActives" id="NextActives" ltype="select">
                  <%                      
                      List<CBaseObject> lstAD = m_WorkflowDef.getActivesDefMgr().GetList();
                      for (CBaseObject obj : lstAD)
                      {
                          CActivesDef ActivesDef = (CActivesDef)obj;
                          if (ActivesDef == m_PreActives)
                              continue;
                          if (ActivesDef.getWType() == ActivesType.Start)
                              continue;
                          %>
                  <option value="<%=ActivesDef.getId()%>"><%=ActivesDef.getName()%></option>
                          <%
                      }
                       %>
                  </select>
                </td>
                <td align="left"></td>
            </tr>
            
        </table>
    </div>
    </form>
</body>
</html>