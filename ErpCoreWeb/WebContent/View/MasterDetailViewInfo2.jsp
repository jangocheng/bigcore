<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewCatalog" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
    
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CView m_View=null;
CTable m_Table =null;
CTable m_DetailTable =null;
CViewDetail m_ViewDetail =null;
String id = request.getParameter("id");
boolean m_bIsNew=false;
if (!Global.IsNullParameter(id))
{
    m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
}
else
{
    m_bIsNew = true;
    if (request.getSession().getAttribute("NewMasterDetailView") == null)
    {
    	response.sendRedirect("MasterDetailViewInfo1.jsp?id=" + request.getParameter("id") + "&catalog_id=" + request.getParameter("catalog_id"));
		return ;
    }
    else
    {
    	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewMasterDetailView");
        m_View =(CView) sortObj.values().toArray()[0];
    }
}
m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
m_ViewDetail = (CViewDetail)m_View.getViewDetailMgr().GetFirstObj();
if (m_ViewDetail != null)
	m_DetailTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_ViewDetail.getFW_Table_id());
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        
    $(function() {
    	
    	LoadData();
    });

    function LoadData()
    {
    	$("#txtMasterTable").val("<%=m_Table!=null?m_Table.getName():""%>");
    	$("#txtDetailTable").val("<%=m_DetailTable!=null?m_DetailTable.getName():""%>");
    }
    
    function onPrev() {
    	var url="MasterDetailViewInfo1.jsp?id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %>";
        document.location.href=url;
    }  
    function onNext() {
    	var MasterColumn= GetMasterColumn();
    	var DetailColumn= GetDetailColumn();
    	if(MasterColumn.length==0)
   		{
   			alert("请选择主表字段！");
   			return ;
   		}
    	if(DetailColumn.length==0)
   		{
   			alert("请选择从表字段！");
   			return ;
   		}
        $.post(
            'MasterDetailViewInfo2.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                MasterColumn:MasterColumn,
                DetailColumn:DetailColumn,
                Action: 'Next'
            },
             function (data) {
                 if (data == "" || data == null) {
                	 var url="MasterDetailViewInfo3.jsp?id=<%=request.getParameter("id")%>&catalog_id=<%=request.getParameter("catalog_id")%>";
                	 document.location.href=url;
                	 return true;
                 }
                 else {
                     return false;
                 }
             },
             'text');
    }  
    function GetMasterColumn()
    {
    	var str = "";
        $("#listMasterColumn input:chekcbox").each(function () {
        	if(this.checked)
            	str += this.value + ",";
        });

        return str;
    }  
    function GetDetailColumn()
    {
    	var str = "";
        $("#listDetailColumn input:chekcbox").each(function () {
        	if(this.checked)
            	str += this.value + ",";
        });

        return str;
    }
    function onCancel() {
        $.post(
            'MasterDetailViewInfo2.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                Action: 'Cancel'
            },
             function (data) {
                if (data == "" || data == null) {
                  	 parent.$.ligerDialog.close();
               	 	return true;
                }
                else {
               		$.ligerDialog.warn(data);
                    return false;
                }
             },
             'text');
    } 
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1">
    <div >
        <p>选择字段：</p>
        <br />
        <table><tr><td valign=top>
        <table cellpadding="0" cellspacing="0" class="l-table-edit" style="height:50px">
            <tr>
                <td align="left">
                    &nbsp;主表：</td>
                <td align="left">
                    &nbsp;
                    <input type="button" id="txtMasterTable" readonly="readonly" /> 
                    </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    &nbsp;字段：</td>
                <td align="left">
                     <table id="listMasterColumn">
                     <%List<CBaseObject> lstObj = m_Table.getColumnMgr().GetList();
                     for (CBaseObject obj : lstObj)
                     {
                         CColumn column = (CColumn)obj;
                         boolean bSelect= (m_View.getColumnInViewMgr().FindByColumn(column.getId()) != null);
                         %>
					    <tr><td> <input type="checkbox" <%if(bSelect){ %>checked<%} %> name="Master" value="<%=column.getId().toString()%>"/></td><td><%=column.getName() %></td></tr>
					 <%} %>
					 </table>
                </td>
            </tr>
                        
        </table>
        </td><td valign=top>
        <table cellpadding="0" cellspacing="0" class="l-table-edit" style="height:50px">
            <tr>
                <td align="left">
                    &nbsp;从表：</td>
                <td align="left">
                    &nbsp;
                    <input type="button" id="txtDetailTable" readonly="readonly" />
                    </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    &nbsp;字段：</td>
                <td align="left">
                     <table id="listDetailColumn">
					    <%List<CBaseObject> lstObj2 = m_DetailTable.getColumnMgr().GetList();
	                     for (CBaseObject obj : lstObj2)
	                     {
	                         CColumn column = (CColumn)obj;
	                         boolean bSelect=(m_ViewDetail.getColumnInViewDetailMgr().FindByColumn(column.getId()) != null);
	                         %>
						    <tr><td> <input type="checkbox" <%if(bSelect){ %>checked<%} %>  name="Detail" value="<%=column.getId().toString()%>"/></td><td><%=column.getName() %></td></tr>
						 <%} %>
					 </table>
                    
                </td>
            </tr>
                        
        </table>
        
        </td></tr></table>
        <br />
        <p>
            <input type="button" id="btNext" style="width:60px" value="上一步" onclick="onPrev();" />
            
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btNext" style="width:60px" value="下一步" onclick="onNext();" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btCancel" style="width:60px" value="取消" onclick="onCancel();" />
            </p>
    </div>
    </form>
</body>
</html>