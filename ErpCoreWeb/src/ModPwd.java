

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CContext;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class ModPwd
 */
@WebServlet("/ModPwd")
public class ModPwd extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	HttpServletResponse response;
       
	public CUser m_User = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModPwd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.request=request;
		this.response=response;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		m_User = (CUser) request.getSession().getAttribute("User");
		
		String sPwd=request.getParameter("txtPwd");
		String sNewPwd=request.getParameter("txtNewPwd");
		String sNewPwd2=request.getParameter("txtNewPwd2");
		
		CContext ctx = Global.GetCtx(this.getServletContext());
        if (ctx == null)
        {
        	response.getWriter().write("<script>alert('单位没有注册！');</script>");
            return;
        }
        if (!m_User.getPwd().equals(sPwd))
        {
        	response.getWriter().write("<script>alert('密码不正确！');</script>");
            return;
        }

        if (!sNewPwd.equals(sNewPwd2))
        {
        	response.getWriter().write("<script>alert('密码不一致！');</script>");
            return;
        }
        m_User.setPwd(sNewPwd);
        m_User.m_ObjectMgr.Update(m_User);
        if (!m_User.m_ObjectMgr.Save(true))
        {
        	response.getWriter().write("<script>alert('修改失败！');</script>");
            return;
        }

        response.getWriter().write("<script>alert('修改成功！');parent.$.ligerDialog.close();</script>");
	}

}
