

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.ErpCoreWeb.Common.EditObject;
import com.ErpCoreWeb.Common.Global;

/**
 * Application Lifecycle Listener implementation class GlobalListener
 *
 */
@WebListener
public class GlobalListener extends HttpServlet implements ServletContextListener, HttpSessionListener {
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GlobalListener() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub
    	//一些产品不需要登录时使用
        arg0.getSession().setAttribute("TopCompany", "ErpCore");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    	//在应用程序启动时运行的代码
        //Global.SetUploadPath(this.getServletContext());
       
        //启动缓存监控
        //StartCtxCacheMonitor();
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub


        EditObject.Cancel(arg0.getSession().getId());
        
    }
	
}
