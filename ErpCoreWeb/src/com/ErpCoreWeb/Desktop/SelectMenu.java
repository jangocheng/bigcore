package com.ErpCoreWeb.Desktop;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CMenu;
import com.ErpCoreModel.UI.CView;
import com.ErpCoreModel.UI.enumMenuType;
import com.ErpCoreModel.UI.enumViewType;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class SelectMenu
 */
@WebServlet("/SelectMenu")
public class SelectMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;
       
    public CCompany m_Company = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectMenu() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        
        CUser user = (CUser)request.getSession().getAttribute("User");
        m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(user.getB_Company_id());

    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";
        
        if (Action.equalsIgnoreCase("GetData"))
        {
        	GetData();
            return ;
        }
	}

    void GetData()
    {
        String pid = request.getParameter("pid");
        UUID Parent_id = Util.GetEmptyUUID();
        if (!Global.IsNullParameter(pid))
            Parent_id = Util.GetUUID(pid);
        String sData = LoopGetMenu( Parent_id);
        
        sData = "[" + sData + "]";

        try {
			response.getWriter().print(sData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    String LoopGetMenu(UUID Parent_id)
    {
    	String sData="";
        List<CBaseObject> lstObj= Global.GetCtx(this.getServletContext()).getMenuMgr().GetList();
        for (int i=0;i< lstObj.size();i++)
        {
            CMenu menu = (CMenu)lstObj.get(i);
            if (!menu.getParent_id().equals(Parent_id))
                continue;
            String sChildren = LoopGetMenu( menu.getId());
            String sIconUrl = String.format("../%s/MenuIcon/default.png",
                Global.GetDesktopIconPathName());
            if (menu.getIconUrl().length()>0)
            {
                sIconUrl = String.format("../%s/MenuIcon/%s",
                    Global.GetDesktopIconPathName(), menu.getIconUrl());
            }
            String url = menu.getUrl();
            if (menu.getMType() == enumMenuType.CatalogMenu)
                url = "SelectMenu.jsp?pid=" + menu.getId().toString();
            else if (menu.getMType() == enumMenuType.ViewMenu)
            {
                CView view = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(menu.getUI_View_id());
                if (view == null)
                    continue;
                if (view.getVType() == enumViewType.Single)
                    url = "../View/SingleView.jsp?vid=" + view.getId().toString();
                else if (view.getVType() == enumViewType.MasterDetail)
                    url = "../View/MasterDetailView.jsp?vid=" + view.getId().toString();
                else
                    url = "../View/MultMasterDetailView.jsp?vid=" + view.getId().toString();
            }
            else if (menu.getMType() == enumMenuType.WindowMenu)
            {
            }
            else if (menu.getMType() == enumMenuType.ReportMenu)
            {
                url = "../Report/RunReport.jsp?id=" + menu.getRPT_Report_id().toString();
            }
            sData += String.format("{\"id\":\"%s\",\"text\":\"%s\",\"icon\":\"%s\",\"mtype\":\"%d\",\"url\":\"%s\", children: [%s]}},",
                menu.getId().toString(), menu.getName(), sIconUrl, menu.getMType().ordinal(), url, sChildren);
        }
        if(sData.length()>0 && sData.endsWith(","))
        	sData = sData.substring(0, sData.length()-1);
        
        return sData;
    }
}
