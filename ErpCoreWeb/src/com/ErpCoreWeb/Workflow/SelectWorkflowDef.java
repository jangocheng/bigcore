package com.ErpCoreWeb.Workflow;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Workflow.CWorkflowDef;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class SelectWorkflowDef
 */
@WebServlet("/SelectWorkflowDef")
public class SelectWorkflowDef extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectWorkflowDef() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        
		
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetData"))
        {
        	GetData();
            return ;
        }
	}

    void GetData()
    {
        String B_Company_id = request.getParameter("B_Company_id");
        String FW_Table_id = request.getParameter("FW_Table_id");
        if (Global.IsNullParameter(B_Company_id)
            || Global.IsNullParameter(FW_Table_id))
            return;

        CCompany Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));
        if (Company == null)
            return;

        String sData = "";
        List<CBaseObject> lstObj = Company.getWorkflowDefMgr().GetList();

        int iCount = 0;
        for (CBaseObject obj : lstObj)
        {
            CWorkflowDef WorkflowDef = (CWorkflowDef)obj;
            if (!WorkflowDef.getFW_Table_id().equals(FW_Table_id))
                continue;

            sData += String.format("{ \"id\": \"%s\",\"Name\":\"%s\"},"
                , WorkflowDef.getId().toString(), WorkflowDef.getName());
            iCount++;
        }
		if (sData.endsWith(","))
			sData = sData.substring(0, sData.length() - 1);
        sData = "[" + sData + "]";
        String sJson = String.format("{\"Rows\":%s,\"Total\":\"%d\"}"
            , sData, iCount);

        try {
			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
